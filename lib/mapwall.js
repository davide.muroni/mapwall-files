let scripts = document.getElementsByTagName('script');
let currentscript = scripts[scripts.length - 1];
const _configFile = currentscript.getAttribute("data-config") || "config.json";
 
/*
 *  Contiene le funzionalità generali non configurabili per l'interfaccia wall
 *
 */
const mapwall_components = {
  mw_warningBox: `
  <div id="mw_warningBox">
    <div style="flex-grow:1;font-size:40px;margin-right:5px;"><i class="fa fa-info-circle"></i></div>
    <div id="mw_wbText"></div>
    <div id="mw_wbButtons" style="display: none;">
      <div class="mw_quest mw_yes">Yes</div>
      <div class="mw_quest mw_no">No</div>
    </div>
    <div id="mw_wbOkButton" style="display:none;">
      <div class="mw_quest mw_ok">Ok</div>
    </div>
  </div>
  `,

  mw_master_popup: `
  <div class="draggable popup100">
  <div class="popTitle"></div>
  <div class="popContent"></div>      
    <div class="container-fluid">
        <ul class="nav nav-pills nav-justified">
          <!-- click on a .showRoad element makes the corresponding map feature blink -->
          <li class="nav-item notremove"><a class="nav-link showRoad" href="#" title="Flash on map"><span class="fa fa-map-marker"></span></a></li>
          <!-- click on a .expandPop element resizes the corresponding popup by twice -->
          <li class="nav-item notremove"><a class="nav-link expandPop" href="#" title="Expand"><span class="fa fa-expand"></span></a></li>
          <!-- click on a .softPop element reduces the background opacity (useful to compare charts referring to different features) -->
          <li class="nav-item notremove"><a class="nav-link softPop" href="#" title="Softening"><span class="fa fa-tint"></span></a></li>
          <!-- click on a .closePop element closes the popup -->
          <li class="nav-item notremove"><a class="nav-link closePop" href="#" title="Close popup"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
  </div>
`
}

var interact = interact.default;

document.oncontextmenu = () => {return false;}
document.addEventListener('DOMContentLoaded', (event) => { 
  
  window.mapwall = new Mapwall() //when ready dispatch mapwallReady event
  const ignoreFromList = 'input, textarea, a[href], button, select, option, .mw_disabledrag'

  interact('.resizable') //deve andare prima del .draggable
  .resizable({
    edges: {left:true, right:true}, //'.resize-top'},
    listeners: {
      move (event) {
        var target = event.target
        var x = (parseFloat(target.getAttribute('data-x')) || 0)
        var y = (parseFloat(target.getAttribute('data-y')) || 0)

        // update the element's style
        target.style.width = event.rect.width + 'px'
        target.style.height = event.rect.height + 'px'

        // translate when resizing from top or left edges
        x += event.deltaRect.left
        y += event.deltaRect.top

        target.style.webkitTransform = target.style.transform =
          'translate(' + x + 'px,' + y + 'px)'

        target.setAttribute('data-x', x)
        target.setAttribute('data-y', y)
      }
    },
    modifiers: [
      // keep the edges inside the parent
      interact.modifiers.restrictEdges({
        outer: 'parent'
      }),

      // minimum size
      interact.modifiers.restrictSize({
        min: { width: 200, height: 150 }
      }),

      interact.modifiers.aspectRatio({
        // make sure the width is always double the height
        ratio: 'preserve'
      })

    ],

    inertia: true
  });


  interact('.draggable').draggable({
    ignoreFrom: ignoreFromList,
    // enable inertial throwing
    inertia: true,
    // keep the element within the area of it's parent
    modifiers: [
      interact.modifiers.restrictRect({
        restriction: 'body',
        endOnly: true
      })
    ],
    // enable autoScroll
    autoScroll: true,

    // call this function on every dragmove event
    onmove: dragMoveListener,
    // call this function on every dragend event
    onend: function (event) {}
  });  
});

const activeBtnBG = "var(--active-bg)";
const normBtnBG = "var(--mapwall-bg)";
const softBG = "var(--mapwall-bg-soft)";
const hiddenStyleUser = new ol.style.Style({});
const hiddenStyleMw = new ol.style.Style({});


const tileMarkerStyle = function (feature, resolution) {
  let col = '#0073e6';
  let radius = 12;
  return new ol.style.Style({
    image: new ol.style.RegularShape({
      stroke: new ol.style.Stroke({color: 'black', width: 1}),
      fill: new ol.style.Fill({color: col}),
      points: 3,
      radius: radius,
      rotation: Math.PI / 3,
      angle: 0,
      displacement: [-radius, 5]
    }),
  })
}


var mw_dblclick_timer = 0;
var mw_prevent = false;
var _lastMovedMap
const SERVICE_LAYER_NAME = "mwServiceLayer"


/**
 * 
 * 
 * MAPWALL MAP CLASS
 * 
 * map and gis related properties and methods
 * 
 */
function MWMap (map, mapAnchorId) {
  /* private methods */
  let getConfigView = () => {
    let sv = globalConfig.geo.interaction.sharedView;
    if(globalConfig.geo.maps.length > 1 && sv != undefined) {
      if( sv.center != undefined
        && sv.center.lat != undefined
        && sv.center.lon != undefined
      ) {
        return sv;
      }
      else throw("Missing sharedView parameters")
    }
    else {
      if(map.view != undefined
        && map.view.center != undefined
        && map.view.center.lat != undefined
        && map.view.center.lon != undefined
      ) {
        return map.view;
      }
      else throw("Missing view parameters for map " + mapAnchorId)
    }
  }

  let getMwView = () => {
    let transformView = (view) => {
      let center = ol.proj.transform([parseFloat(view.center.lon), parseFloat(view.center.lat)], 'EPSG:4326', 'EPSG:3857');
      return new ol.View({
        center:center,
        zoom: view.zoom ? parseFloat(view.zoom) : 10,
        minZoom: view.minZoom ? parseFloat(view.minZoom) : undefined,
        maxZoom: view.maxZoom ? parseFloat(view.maxZoom) : undefined,
      })
    }
    
    let sv = globalConfig.geo.interaction.sharedView;
    if(globalConfig.geo.maps.length > 1 && sv != undefined) {
      if(window.mw_shared_view == undefined) {
        window.mw_shared_view = transformView(getConfigView());
      }
      return window.mw_shared_view;
    }
    else if(map.view != undefined) {
      return transformView(map.view)
    }
    else throw("No view defined for map " + mapAnchorId);
  }

  let setTileRetry = (source) => {
    source.on("tileloaderror", (evt) => {      
      if(evt.tile.tries == undefined) //infinite loops handler
        evt.tile.tries = 2;
      else
        evt.tile.tries--

      if(evt.tile.tries > 0)
        evt.tile.load();
    });
  }

  let getFeaturePopup = (type, popupCandidate) => {
    let elementPopup = null;
    if(!popupCandidate) {
      elementPopup = (new DOMParser()).parseFromString(mapwall_components.mw_master_popup, 'text/html').body.firstChild;
    }
    else if(type && popupCandidate) {
      
      switch(type) {
        case 'id':
          let master = document.getElementById(popupCandidate)
          if(master == null) {
            let msg = "Popup element " + popupCandidate + " not found"
            console.warn(msg)
          }
          else elementPopup = master.cloneNode(true)
          break;
  
        case 'popup':
          if(typeof popupCandidate == 'string') {
            try {
              elementPopup = (new DOMParser()).parseFromString(popupCandidate, 'text/html').body.firstChild;
            }
            catch(e) {
              console.warn(e)
            }
          }
          else if(popupCandidate instanceof Element) {
            elementPopup = popupCandidate;
          }
          break;
  
        default:
          break;
      }
    }
    return elementPopup
  }

  let showHideFeatures = (show, layerName, propertyName, propertyValue) => {
    let layer = this.getLayerByName(layerName);
    if(layer instanceof ol.layer.Vector) {
      layer.getSource().getFeatures().forEach((f) => {
        let styleNow = f.getStyle();
        if(styleNow !== hiddenStyleMw) { //Bisogna distinguere altrimenti toggleUnwatchedFeatures rivisualizza feature nascoste da toggleFeatures
          let doIshow = (show == undefined) ? (styleNow == hiddenStyleUser) : show

          let newStyle = doIshow ? undefined : hiddenStyleUser;
          if(propertyValue != undefined && propertyName != undefined) {
            if(f.get(propertyName) == propertyValue) {
              f.setStyle(newStyle)
            }
          }
          else f.setStyle(newStyle)
        }
      })
    }
    else console.warn("Method available on Vector layers only")
  }

  let isListening = (name) => {
    /*
    if(listeners.length == 0) { //se non è definito un listenerLayer restituisce true solo per il Tile
      let layer = this.getLayerByName(name);
      if(layer instanceof ol.layer.Tile && name == map_mapwall.getLayers().item(map_mapwall.getLayers().getLength()-1).get("name")) {
        return true;
      }
    }
    */
    if(listeningLayersEnabled == false) 
      return false
    else
      return listeningLayers.length == 0 || listeningLayers.indexOf(name) != -1
  }

  //handler click on map
  let clickAction = (evt) => {
    evt["lat"] = evt.coordinate[1];
    evt["lon"] = evt.coordinate[0];

    let tileCallbacks = []
    map_mapwall.forEachLayerAtPixel(evt.pixel, (layer, color) => {
      if(layer.get("name") != SERVICE_LAYER_NAME) {
        if(layer instanceof ol.layer.Tile) { //Tile sources don't have features
          let layerName = layer.get("name")
          let zindex = this.getLayerByName(layerName).getZIndex() || 0; //0 is the lowest

          if(isListening(layerName) && this.getLayerByName(layerName).getVisible() == true) {
            if(layersMap[layerName] && layersMap[layerName].callback && !(layersMap[layerName].popup || layersMap[layerName].popupId)) {
              tileCallbacks.push({
                callback: layersMap[layerName].callback,
                pars:[{lat:evt.lat, lon:evt.lon}, evt, layerName], //first par for custom callback is a lat lon object
                zindex:zindex
              })
            }
            else {
              let tilePointId = ((evt.lat.toFixed(4) + '-' + evt.lon.toFixed(4)) + '_' + layerName).replace(/[^a-zA-Z0-9-_]/g,'_');
              tileCallbacks.push({callback:"showPopup", pars:[tilePointId, evt, layerName], zindex:zindex})
            }
          }
        }
      }
    },
    //options
    {
      hitTolerance: 20,
      layerFilter: (layer) => {
        if(ol.layer.WebGLPoints !== undefined)
          return layer.get("name") != undefined
            && layer.get("name") != SERVICE_LAYER_NAME
            && !(layer instanceof ol.layer.WebGLPoints)
        else 
          return layer.get("name") != undefined
            && layer.get("name") != SERVICE_LAYER_NAME
      }
    })

    let featuresCallbacks = [];
    let clickedFeatures =map_mapwall.getFeaturesAtPixel(evt.pixel)

    if(clickedFeatures && clickedFeatures.length > 0) {
      clickedFeatures.forEach((f) => {
        let featureType = f.get("type");
        
        if(isListening(featureType) && featureType != undefined) { 
          //se listeninglayers è vuoto restituisce true, le callback vengono messe tutte in coda, 
          //dopo verrà eseguita solo quella in cima in base a OL zindex
          let fid = f.getId()
          let zindex =  this.getLayerByName(featureType).getZIndex() || 0;
          let callback = "showPopup";
          
          if(layersMap[featureType] && layersMap[featureType].callback)
            callback = layersMap[featureType].callback;

          featuresCallbacks.push({callback: callback, pars:[fid, evt, featureType], zindex:zindex})
        }
      })
    }

    let callbacks = featuresCallbacks.concat(tileCallbacks);
    callbacks.sort((a,b) => { //reverse order, greatest zindex first
      if(a.zindex < b.zindex) return 1
      if(a.zindex > b.zindex) return -1
      return 0
    })

    if(callbacks.length > 0 && listeningLayersEnabled == true) {
      if(listeningLayers.length == 0) {
        callbacks.splice(1); //esegue quella in cima in base a zindex
      }
      
      let callbackEvent = new Event('mwExecuteCallback')
      callbackEvent.mapwall_data = {
        mapId: mapAnchorId,
        callbacks:callbacks
      };
      document.dispatchEvent(callbackEvent)
    }
  }
  /* end private methods */
  
  let listeningLayers = [];
  let listeningLayersEnabled = true;
  let layersMap = {};
  let map_mapwall = undefined;
  let attributions = map.attribution && map.attribution != '' ? map.attribution : undefined;
  let baseLayer = {};
  let servicelayer = null;
  let basemap = map.basemap || undefined; //if undefined uses OSM standard basemap
  if(basemap && basemap.src && basemap.type) {
    let source = undefined;
    switch(basemap.type) {
      case "OSM":
        source = new ol.source.OSM({
          url:basemap.src,
          attributions: attributions
        });
        setTileRetry(source);
        baseLayer = new ol.layer.Tile({source: source});
        break;

      case "XYZ": //TODO tileUrlFunction support?? see https://openlayers.org/en/latest/apidoc/module-ol_source_XYZ-XYZ.html
        source = new ol.source.XYZ({
          url:basemap.src,
          attributions: attributions
        });
        setTileRetry(source);
        baseLayer = new ol.layer.Tile({source: source}); //define url template {x}/{y}/{z} in the src config param 
        break;

      case "WMS":
        source = new ol.source.TileWMS({
          url: basemap.src,
          attributions: attributions,
          crossOrigin: 'anonymous',
          params: {
            'TILED': true,
            "CRS":"EPSG:3857"
          },
        })
        setTileRetry(source);
        baseLayer = new ol.layer.Tile({
          source: source
        })
        break;
      default:
        break;
    }
  }
  else baseLayer = new ol.layer.Tile({
    source: new ol.source.OSM({attributions: attributions})
  });

  let isShared = globalConfig.geo.interaction && globalConfig.geo.interaction.sharedView;
  let startLinked = (isShared
    && globalConfig.geo.interaction.sharedView.startLinked 
    && globalConfig.geo.interaction.sharedView.startLinked == true) 
    || (isShared && globalConfig.geo.interaction.sharedView.startLinked == undefined);
    
  map_mapwall = new ol.Map({
    view: (isShared && !startLinked) ? new ol.View(Object.assign({}, getMwView().getProperties())) : getMwView(),
    layers: [baseLayer],
    interactions : ol.interaction.defaults({
      doubleClickZoom :false, 
      altShiftDragRotate:false, 
      pinchRotate:false
    }),
    controls : ol.control.defaults({
      attribution:true,
      zoom : false
    }),
    target: mapAnchorId
  });
  
  /**
   *  Map Events
   */ 
  map_mapwall.on("click", function(evt) {
    //Aspetta per verificare non sia un doppio click
    mw_dblclick_timer = setTimeout(function() {
      if (!mw_prevent) {
        clickAction(evt);
      }
      mw_prevent = false;
    }, 300);
  });

  map_mapwall.on("moveend", function(evt) {
    _lastMovedMap = mapAnchorId;
  })
  
  /**
   * Return the mapwall ol.map object
   */
  this.getMap = () => {
    return map_mapwall;
  }

  /**
   * 
   */
  this.getView = () => {
    return map_mapwall.getView();
  }

  /**
   * 
   */
  this.setView = (view) => {
    if(view instanceof ol.View)
      map_mapwall.setView(view);
    else
      throw("Invalid OL view")
  }

  /**
   * Delete a layer from the map
   * 
   * @param {*} lname The name of the layer to be deleted
   */
  this.removeLayer = (lname) => {
    let layer = this.getLayerByName(lname)
    if(layer != null) {
      if(layersMap[lname]) delete layersMap[lname]
      this.getMap().removeLayer(layer)
    }
  }

  /**
   * Add a new layer to the OL map, the name is the name of the layer and is set as 
   * "type" attribute for each feature of the layer. The optional "par" parameter could contain the name of a custom callback
   * or an object like this: {callback:"callbackName", popupId:"customPopupId", zindex:2, popup:'Element or HTML', filter:'filterMethodName'} 
   * where popupId defines a custom template to clone when showing a popup on click on this layer, zindex is used for setting the layer zindex 
   * in the OL stack and popup, popup can contain an HTML string or a DOM Element representing your custom popup, filter is the name of a callback
   * that will be called before showing the popup: if filter returns true, 
   */
  this.addLayer = (layer, name, par) => {
    layer.set("name", name);
    
    if(!layersMap[name]) layersMap[name] = {}

    if(layer instanceof ol.layer.Vector || layer instanceof ol.layer.WebGLPoints) { //Supporto sperimentale webgl
      layer.getSource().getFeatures().forEach((f, i) => {
        if(!f.getId()) f.setId("mw_"+ name + "_" + i);
        f.set("type",name)
      })
    }
    else if(layer instanceof ol.layer.Tile) {
      let source = layer.getSource();
      setTileRetry(source);
      if(par.placemarker && par.placemarker == true) {
        layersMap[name].placemarker = true;
        if(servicelayer == null) {
          servicelayer = new ol.layer.Vector({
            name: SERVICE_LAYER_NAME,
            zIndex: 1000,
            source: new ol.source.Vector({
              features: []
            }),
            style: tileMarkerStyle
          })
          map_mapwall.addLayer(servicelayer);
        }
      }
    }
    
    if(par && (typeof par == 'string' || par instanceof Function)) {
      if(!layersMap[name]) layersMap[name] = {}
      layersMap[name].callback = checkCallbackType(par); //callback personalizzata (prima featureType in config.json)
    }

    else if(par && (par.callback || par.popupId || par.zindex || par.popup || par.filter)) {
      if(!layersMap[name]) layersMap[name] = {};
      if(par.callback) layersMap[name].callback = checkCallbackType(par.callback);
      if(par.filter) layersMap[name].filter = checkCallbackType(par.filter);
      if(par.popupId && ! par.popup) { //par.popup ha precedenza
        layersMap[name].popupId = par.popupId; //per getMasterList
        layersMap[name].popup = getFeaturePopup('id', par.popupId); //layersMap.popup c'è sempre!
      }
      if(par.popup) layersMap[name].popup = getFeaturePopup('popup', par.popup);
      if(par.zindex) layer.setZIndex(parseInt(par.zindex));
    }

    //Default popup if no callback and no custom popup
    if(!layersMap[name].popup && !layersMap[name].callback) {
      layersMap[name].popup = getFeaturePopup();
    }
    map_mapwall.addLayer(layer)
  }

  /* Return the layer mapping */
  this.getLayersMap = () => {return layersMap}

  /**
   * Return the ol.layer object by the name passed in addLayer()
   */
  this.getLayerByName = (name, cb) => {
    let layers = map_mapwall.getLayers()
    for(let i=0; i<layers.getLength(); i++) {
      let layer = layers.item(i);
      if(layer.get('name') != undefined & layer.get('name') === name) {
        if(cb) return cb(layer);
        else return layer
      }
    }
    if(cb) return cb(null);
    else return null
  },

  /**
   * Return the ol.source of a ol.layer, useful if you need a getFeatureById() operation
   */
  this.getSourceByLayerName= (name, cb) => {
    let layer = this.getLayerByName(name)
    if(cb) return cb(layer.getSource())
    else return layer.getSource();
  },

  /**
   * Return the "id" feature object belonging to the "name" layer or null if not present
   */ 
    this.getFeatureById = (lname, id, cb) => {
    let f = null;
    let layer = this.getLayerByName(lname);
    if(layer instanceof ol.layer.Tile) {
      if(layer.mwFeatures && layer.mwFeatures[id]) {
        f = layer.mwFeatures[id]
      }
    }
    else if(layer instanceof ol.layer.Vector) {
      f = this.getSourceByLayerName(lname).getFeatureById(id);
    }
    if(cb) return cb(f);
    else return f;
  }

  /**
   * Add a new feature to a layer, if the feature ID of a Vector layer is not set, timestamp is used
   */ 
    this.addFeatureToLayer = (lname, feature, cb) => {
    let layer = this.getLayerByName(lname);
    if(layer instanceof ol.layer.Tile) {
      if(!feature.mwid && !feature.id) {
        throw("Missing id for the feature")
      }
      if(!layer["mwFeatures"]) layer["mwFeatures"] = {}
      let id = feature.mwid ? feature.mwid : feature.id
      layer["mwFeatures"][id] = feature;
    }
    else if(layer instanceof ol.layer.Vector) {
      if(!feature.getId()) feature.setId("mw_" + lname + "_" + Date.now());
      feature.set("type", lname);

      let source = this.getSourceByLayerName(lname);
      source.addFeature(feature);
    }

    if(cb) return cb();
    else return;
  }

  /**
   * Remove a feature from a layer
   */ 
  this.removeFeatureFromLayer = (lname, featureId, cb) => {
    let f = this.getFeatureById(lname, featureId);
    if(f != null) {
      let layer = this.getLayerByName(lname);
      if(layer instanceof ol.layer.Tile) {
        delete layer.mwFeatures[featureId]
      }
      else if(layer instanceof ol.layer.Vector) {
        let source = this.getSourceByLayerName(lname);
        source.removeFeature(f);
      }
    }
    else console.warn("removeFeatureFromLayer: Feature " + featureId + " not found")
  
    if(cb) return cb();
    else return;
  }

/**
 * Add a batch of features to the map layer
 * @param {*} lname Layer name
 * @param {*} features Array of features
 * @param {*} cb callback to execute when finished (optional)
 * @returns 
 */
 this.addFeaturesToLayer = (lname, features, cb) => {
  features.forEach((f, i) => {
    if(!f.getId()) f.setId("mw_" + lname + "_" + i)
    this.addFeatureToLayer(lname, f)
  })

  if(cb) return cb()
  else return;
}


  /**
   * Return the name list of loaded layers
   */
  this.getLayersNames = (cb) => {
    let layers = []
    map_mapwall.getLayers().forEach((l) => {
      if(l.get("name") && l.get("name") != SERVICE_LAYER_NAME) layers.push(l.get("name"))
    })
    if(cb) return cb(layers)
    else return layers;
  }

  /**
   * Hide features with propertyName == propertyValye and belonging to "layerName" 
   * If propertyName and propertyValue are not set 
   * ONLY VECTOR
   */
  this.hideFeatures = (layerName, propertyName, propertyValue) => {
    showHideFeatures(false, layerName, propertyName, propertyValue)
  }

  /**
   * Hide features with propertyName == propertyValye and belonging to "layerName" 
   * If propertyName and propertyValue are not set 
   * ONLY VECTOR
   */
  this.showFeatures = (layerName, propertyName, propertyValue) => {
    showHideFeatures(true, layerName, propertyName, propertyValue)
  },

  /**
   * Toggle features with propertyName == propertyValye and belonging to "layerName" 
   * If propertyName and propertyValue are not set 
   * ONLY VECTOR
   */
  this.toggleFeatures = (layerName, propertyName, propertyValue) => {
    showHideFeatures(undefined, layerName, propertyName, propertyValue)
  }

  /**
   * 
   * @param {*} fid The feature ID
   * @param {*} layerName The layer name
   * 
   * Makes a feature blinking on the map. Works with Vector layers only
   * ONLY VECTOR
   */
  this.blinkFeature = (fid, layerName) => {
    let layer = this.getLayerByName(layerName);
    if(layer instanceof ol.layer.Vector) {
      let f = this.getFeatureById(layerName, fid);
      let x = 0;
      let intBlink = setInterval(() => {
        x += 500;
    
        if(x >= 5000) {
          clearInterval(intBlink);
          f.setStyle()
        }
        else {
          if(x % 1000 != 0) {
            f.setStyle(hiddenStyleMw);
          } 
          else {
            f.setStyle()
          } 
        }
      } ,500)
    }
    else console.warn("Method available on Vector layers only")
  },

  /**
   * Show a layer if hidden or hide if visible
   */
  this.toggleLayer = (name) => {
    if(name) {
      let l = this.getLayerByName(name);
      let viz = l.getVisible() ? false : true;
      l.setVisible(viz);
      return viz
    }
    else throw("Missing Layer Name")
  }

  /**
   * Moves the map view to the specified coordinates and zoom on. assumes EPSG 4326 by default (most used)
   */
  this.moveTo = (coords, args) => { 
    if(!coords) {
      let v = getConfigView()
      coords = [v.center.lon, v.center.lat]
      args = {zoom:v.zoom}
    }
    if(!Array.isArray(coords)) throw("moveTo error: wrong coordinates")
    if(!args) args = {epsg:4326};
    if(!args.epsg) args.epsg = 4326;

    let center = args.epsg && (parseInt(args.epsg) == 3857) ? coords : ol.proj.transform(coords, 'EPSG:' + args.epsg, 'EPSG:3857');
    map_mapwall.getView().animate({center:center, zoom: args.zoom ? args.zoom : 12, duration: 1500});
  }

  /**
   * Set the list of layers listening for a click. If unset all clicks are supposed to be executed on 
   * the top level Tile layer or the highest feature found in the stack.
   * If names is undefined, listeningLayers is reset
   */
  this.setListeningLayers = (names) => {
    if(typeof names == 'string') names = [names];
    if(names && !Array.isArray(names)) {
      console.warn("setListeningLayers(): Invalid argument")
      return;
    }
    
    listeningLayers = [];
    if(names) {
      names.forEach((name) => {
        this.getLayerByName(name, (l) => {
          if(l == null) console.warn("Layer '"+name+"' not found!")
          else listeningLayers.push(name);
        })
      })
    }
  }

  /**
   * Return the list of the layers listening for a click
   */
  this.getListeningLayers = () => {
    return listeningLayers;
  }

  this.disableListeningLayers = () => {
    listeningLayersEnabled = false;
  }

  this.enableListeningLayers = () => {
    listeningLayersEnabled = true;
  }
} //MWMap



/**
 * 
 * 
 * MAPWALL APPLICATION CLASS
 * 
 * UI properties and methods
 * 
 */
function Mapwall() {
  let mapwall_maps = {};

  ////////////////////////////////////////////////////////////////
  // API public methods
  ////////////////////////////////////////////////////////////////

  /**
   * Return the list of refreshable open popups, means those open ones without the .mw_disableupdate class
   */
  this.getUpdatablePopups = (cb) => {
    let masters = getMasterList().map(m => ":not(#"+m).join(")");
    let selectorQuery = '.mw_openpopup:not(#masterPop):not(.mw_disableupdate):not(.fixedpanel):not(.norefresh)' + masters;
    let ups = document.querySelectorAll(selectorQuery)
    if(cb) return cb(ups)
    else return ups
  }

  /**
   * Return the list of all the open popups
   */
  this.getOpenPopups = (cb) => {
    let masters = getMasterList().map(m => ":not(#"+m).join(")");
    let selectorQuery = '.mw_openpopup:not(#masterPop):not(.fixedpanel)' + masters;
    let ops = document.querySelectorAll(selectorQuery)
    if(cb) return cb(ops)
    else return ops
  }

  /**
   * 
   * @param {*} msg The text to display into the box
   * @param {*} confirm True if you want to show YES and NO button
   * @param {*} cb The callback to execute on YES, only if confirmation is enabled
   * 
   * Shows a warning box on top of the page. If the confirm parameter is set to true, 
   * two buttons are shown: YES and NO. By clicking on YES mapwall executes the callback defined
   * in the third parameter. NO just close the warning box
   */
  this.activateWarningBox = (msg, confirm, cb) => {
    let wb = document.getElementById("mw_warningBox");
    if( wb == null) {
      wb = getHtmlComponent("mw_warningBox");
      document.body.appendChild(wb)
    }

    if(!wb.style.animationName || wb.style.animationName == 'slide-up') {
      wb.querySelector("#mw_wbText").innerText = msg

      if(confirm && confirm==true) {
        wb.querySelector("#mw_wbButtons").style.display = 'flex';
        wb.querySelector("#mw_wbOkButton").style.display = "none";
      }
      else {
        wb.querySelector("#mw_wbButtons").style.display = 'none';
        document.getElementById("mw_wbOkButton").style.display = "flex";
      }
      
      wb.querySelectorAll(".mw_quest").forEach(confBtn => {
        confBtn.addEventListener("click", (e) => {
          let choice = e.currentTarget.classList.contains("mw_yes"); //esegue callback solo su YES, NO chiude il box
          if(choice) {
            wb.remove();
            if(cb) cb();
          }
          this.deactivateWarningBox(); //WARNING ambiguità, si chiude ma non restituisce false!!!
        }, {once:true})
      })

      wb.style.animationName = "slide-down";
      wb.style.top = 0
      return true; //visible
    }
    else return this.deactivateWarningBox() //chiamando 2 volte activate il box si chiude
  }
    
  /**
   * Enable/disable sharedview in a multimap application, using the last moved map
   */
  this.toggleLastSharedView = () => {
    if(mapwall_maps[Object.keys(mapwall_maps)[0]].getView() === window.mw_shared_view)
      unshareView()
    else
      shareLastView()
  } 

  /** 
   * Enable/disable sharedview in a multimap application, based on the interaction/sharedView global property
   */
  this.toggleSharedView = () => {
    if(window.mw_shared_view) {
      Object.keys(mapwall_maps).forEach(k => {
        let m = mapwall_maps[k];
        if(m.getView() !== window.mw_shared_view) {
          m.setView(window.mw_shared_view)
        }
        else {
          m.setView(new ol.View(window.mw_shared_view.getProperties()))
        }
      })
    }
  }

  /**
   * public but not documented methods
   */
  this.deactivateWarningBox = () => {
    let wb = document.getElementById("mw_warningBox")
    if(wb != null) {
      if(wb.style.animationName && wb.style.animationName == "slide-down") {
        wb.style.animationName = "slide-up"
        wb.style.top = -70
      }
      wb.remove();
    }
    return false; //not visible
  }

  /**
   * 
   * Returns the list of map id based on the config.json list
   * 
   */
  this.getMapList = () => {
    let maplist = [];
    window.globalConfig.geo.maps.forEach(m => {
      maplist.push(m.id)
    })
    return maplist;
  }


  this.closePopups = () => {
    removePopups();
  }

  this.closeMapPopups = (mapId) => {
    removePopups({mapId:mapId})
  }

  this.removePopups = (popupId) => {
    if(popupId != undefined) removePopups({popupId: popupId})
    else removePopups();
  }

  this.togglePopups = (btnId) => {
    let active = false;
    mapwall.getOpenPopups((pops) => {
      if(pops.length > 0) active = toggleCtlButtons(btnId);
      pops.forEach(p => {
        p.style.display = p.style.display == 'block' && active ?  'none' : 'block';
      })
    })
  }

  this.toggleUnwatchedFeatures = (btnId) => {
    try {
      let btnHide = document.querySelector("#" + btnId);

      if(btnHide && btnHide.getAttribute("data-toggle") == "true") {
        Object.keys(mapwall_maps).forEach(mk => {
          let m = mapwall_maps[mk];
          m.getMap().getLayers().forEach((l) => {
            if(l.get("name")) { //se è un layer registrato
              if(l instanceof ol.layer.Vector) {
                l.getSource().getFeatures().forEach((f) => {
                  if(f.getStyle() === hiddenStyleMw)
                    f.setStyle() //torna a stile del layer
                })
              }
            }
          })
        })
        
        btnHide.removeAttribute("data-toggle");
        btnHide.style.backgroundColor = normBtnBG
      }
      else {
        let watchedF = {};
        let ididit = false;
        this.getOpenPopups((pops) => {
          pops.forEach((fDiv) => {
            let mapName = fDiv.getAttribute("data-map-id")
            let layerName = fDiv.getAttribute("data-feature-type")
            if(!watchedF[mapName]) watchedF[mapName] = {};
            if(!watchedF[mapName][layerName]) watchedF[mapName][layerName] = [];
            watchedF[mapName][layerName].push(fDiv.getAttribute("data-feature-id"))
          })

          Object.keys(watchedF).forEach(mname => {
            Object.keys(watchedF[mname]).forEach(lname => {
              mapwall_maps[mname].getSourceByLayerName(lname, (source) => {
                source.getFeatures().forEach((ft) => {
                  if(ft.getStyle() !== hiddenStyleUser) {
                    if(watchedF[mname][lname].indexOf(""+ft.getId()) == -1 && watchedF[mname][lname].length > 0){
                      ft.setStyle(hiddenStyleMw); //assegna stile invisibile alla feature
                      ididit = ididit ? ididit : true;
                    }
                  }
                });
              })
            })
            
            if(ididit) {
              btnHide.setAttribute("data-toggle","true");
              btnHide.style.backgroundColor = activeBtnBG
            }
          })
        })
      }
    }
    catch(e) {console.warn("toggleUnwatchedFeatures: nothing to do")}
  }

  this.toggleLinkMenu = (btnId) => {

    let links = document.createElement("div");
    links.id = "linkmenu";
    links.classList = "list-group";
    links.style.position = "absolute";

    globalConfig.links.forEach((l, i) => {
      let newLink = document.createElement("a") //(i == globalConfig.links.length - 1) ? link : link.cloneNode(true);
      newLink.classList = "list-group-item"
      newLink.setAttribute("href", l.url);
      let label = document.createTextNode(' ' + l.label)
      let icon = document.createElement("span")
      icon.classList =  l.classIcon
      newLink.appendChild(icon)
      newLink.appendChild(label)
      links.appendChild(newLink)
    })
    document.getElementById("ctlmenu").appendChild(links)

    let lmenu = document.getElementById("linkmenu")
    let showBtn = document.getElementById(btnId)

    if(lmenu.style.display == 'block') {
      lmenu.style.display = 'none'
      showBtn.style.backgroundColor = normBtnBG;
    }
    else {
      lmenu.style.display = 'block'
      showBtn.style.backgroundColor = activeBtnBG;
      
      let ctlmenu = document.getElementById("ctlmenu");      

      let top = parseInt(ctlmenu.style.top) + (ctlmenu.getAttribute("data-y") ? parseInt(ctlmenu.getAttribute("data-y")) : 0); 
      if(lmenu.offsetHeight + ctlmenu.offsetHeight + top > document.body.offsetHeight) {
        lmenu.style.top = - (lmenu.offsetHeight + 20)
      }
      else {
        lmenu.style.top = ctlmenu.offsetHeight
      }
      
      lmenu.left = (ctlmenu.offsetWidth / 2) - (lmenu.offsetWidth / 2)
    }
  }

  this.goHome = () => {
    window.location.href = globalConfig.homeLink;
  }

  /**
   * reposition panels on the original position if moved by user
   */
  this.restoreWindow = () => {
    Object.keys(mapwall_maps).forEach(k => {
      let m = mapwall_maps[k];
      m.moveTo();
    });

    let panels = document.querySelectorAll(".fixedpanel")
    if(panels.length > 0) {
      panels.forEach((p) => {
        p.style.transform = 'translate(0px, 0px)'; 
        p.style["-webkit-transform"] = 'translate(0px, 0px)';
        //restore original size
        p.style.width = p.getAttribute("data-w-orig");
        p.style.height = p.getAttribute("data-h-orig");
        p.removeAttribute("data-x")
        p.removeAttribute("data-y")
      })
    }
  }

  this.toggleAppMenu = () => {
    let appmenu = document.getElementById("appmenu")
    //let showbtn = document.getElementById("showmenu")
    if(!appmenu.style.display || appmenu.style.display == 'none') {
      appmenu.style.display = 'block'
      
      let ttop =  parseInt(ctlmenu.getAttribute("data-y"));
      let tleft = parseInt(ctlmenu.getAttribute("data-x"));

      let top = ctlmenu.offsetTop - (10 + appmenu.offsetHeight);
      let left = ctlmenu.offsetLeft - Math.trunc(appmenu.offsetWidth/2 - ctlmenu.offsetWidth/2);

      if ((top + ttop) <= 0) ttop = ttop + appmenu.offsetHeight + ctlmenu.offsetHeight + 10; //evt.clientY
      if ((left + tleft) < 0) tleft = -left + 10;
      if (left + tleft + appmenu.offsetWidth > window.innerWidth) tleft = tleft - ((left + tleft + appmenu.offsetWidth) - window.innerWidth) - 10;
      
      appmenu.style.top = top
      appmenu.style.left = left

      appmenu.setAttribute("data-x", tleft)
      appmenu.setAttribute("data-y", ttop)

      appmenu.style.transform = 'translate('+tleft+'px, '+ttop+'px)';

      document.dispatchEvent(new Event('appmenuShow'));
    }
    else {
      appmenu.style.display = 'none'
      appmenu.style.transform = 'translate(0px, 0px)';
      appmenu.style["-webkit-transform"] = 'translate(0px, 0px)';
      appmenu.removeAttribute("data-x")
      appmenu.removeAttribute("data-y")
    }
  }

  //should be private
  this.showPopup = async (id, evt, type) => {

    //Execute a filter, if filter returns false the popup is not shown, if 'string' it is assigned as id for the feature 
    //useful for tile layers, where you can emulate a vector layer simply using this and addFeatureToLayer
    let layersMap = this.getMap(evt.mapwall_data.mapId).getLayersMap();
    let markerForTile = null;

    if(layersMap[type].filter) {
      let filter = layersMap[type].filter;
      evt.mapwall_data = Object.assign(evt.mapwall_data, {feature_id: id, layer: type})
      let filterResult = await filter(evt);
      if(filterResult == false) return;
      if(typeof filterResult == 'string') id = filterResult; //WARNING!!! in vector layer will overwrite the feature id! can be used for clustering
    }

    if(typeof id != 'string' && typeof id != 'number') {
      id = "no_id_popup"
    }

    let popId = ("mw_popup_" + id).replace(/[^a-zA-Z0-9-_]/g,'_');
    
    //ADD Placemarker
    if(layersMap[type].placemarker == true) {
      markerForTile = new ol.Feature({geometry: new ol.geom.Point(evt.coordinate)})
      markerForTile.setId(popId); //for blinking
      getServiceLayer(evt.mapwall_data.mapId).getSource().addFeature(markerForTile)
    }

    if(document.querySelector('#' + popId) != null) return;

    let elementPopup = layersMap[type].popup.cloneNode(true);

    elementPopup.id = popId
    elementPopup.style.zIndex = 998
    elementPopup.style.display = 'block';
    elementPopup.classList += ' mw_openpopup'; //per getOpenPopups e getUpdatablePopups

    elementPopup.setAttribute("data-feature-type", type)  
    elementPopup.setAttribute("data-feature-id", id)
    elementPopup.setAttribute("data-map-id", evt.mapwall_data.mapId)
    elementPopup.setAttribute("data-click-coordinate", evt.coordinate)

    document.body.appendChild(elementPopup)
    let pointEvent = evt.pointerEvent !== undefined ? evt.pointerEvent : evt.originalEvent;
    this.repositioning(elementPopup, pointEvent);

/* TODO verificare!!!!!!
    if(window.globalConfig.popupCallback && window.globalConfig.popupCallback != 'fillPopup') { 
      let popupCallback = window[window.globalConfig.popupCallback];
      if(!popupCallback) throw("Popup callback " +window.globalConfig.popupCallback+ " not defined!")
      window[window.globalConfig.popupCallback](id, elementPopup);
    }
  */
    //se specificati sia popup che callback nell'addLayer, callback è eseguita al posto di fillPopup, 
    //per riempire il popup
    if(layersMap[type].popup && layersMap[type].callback) {
      layersMap[type].callback(id, elementPopup);
    }
    else fillPopup(id, elementPopup);

    //must be done after fillpopup, size may change
    let w = getComputedStyle(elementPopup).getPropertyValue("width")
    let h = getComputedStyle(elementPopup).getPropertyValue("height")
    elementPopup.setAttribute("data-size",parseInt(w)+'x'+parseInt(h))

    ////////////////////////////////////////
    //popup default buttons event listeners
    ////////////////////////////////////////
    let showFeature = elementPopup.querySelector(".showRoad")
    if(showFeature) {
      showFeature.addEventListener("click", (e) => {
        blinkWatchedFeatures(elementPopup)
      })
    }

    let closePop = elementPopup.querySelector(".closePop")
    if(closePop) {
      closePop.addEventListener("click", (e) => {
        this.removePopups(popId)
      })
    }
    
    let softPop = elementPopup.querySelector(".softPop");
    if(softPop) {
      softPop.addEventListener("click", (e) => {
        softBg(elementPopup)    
      })
    }

    let expandPop = elementPopup.querySelector(".expandPop")
    if(expandPop) {
      expandPop.addEventListener("click", (e) => {
        expandPopup(elementPopup)
      })
    }
  }
  

  //must be called when the element is visible 
  this.repositioning = (el, evt, offset)  => {
    let h = parseInt(el.style.height);
    let w = parseInt(el.style.width);
    
    el.style.transform = 'translate(0px, 0px)';
    el.style["-webkit-transform"] = 'translate(0px, 0px)';
    el.removeAttribute("data-x")
    el.removeAttribute("data-y")

    let offsetTop = offset && offset.top ? offset.top : 0;
    let offsetLeft = offset && offset.left ? offset.left : 0;
    if(isNaN(h) || isNaN(w)) {
      let s = window.getComputedStyle(el)
      h = parseInt(s.height)
      w = parseInt(s.width)
    }

    let top = ((evt.clientY + h) > window.innerHeight ? 
    (window.innerHeight - h) : evt.clientY);

    let left = ((evt.clientX + w + offsetLeft) > window.innerWidth ? 
    (window.innerWidth - w) : evt.clientX);
    el.style.top = Math.trunc(top + offsetTop); 
    el.style.left = Math.trunc(left + offsetLeft);
  }

  this.getMap = (id) => {
    if(!id) {
      if(Object.keys(mapwall_maps).length == 1) {
        return mapwall_maps[Object.keys(mapwall_maps)[0]];
      }
      else throw("getMap: missing map ID")
    }
    else {
      return mapwall_maps[id]    
    }
  }

  this.spinnerOn = () => {
    let spinner = document.createElement("div")
    spinner.id = "mw_spinner";
    let spinnerInner2 = document.createElement("div")
    spinnerInner2.classList = "mw_loader"
    spinner.appendChild(spinnerInner2);
    spinner.style.zIndex = 1000;

    
    document.body.appendChild(spinner);
  }


  this.spinnerOff = () => {
    setTimeout(() => {
      document.getElementById("mw_spinner").remove();  
    }, 1000);
  }
  ////////////////////////////////////////////////////////////////
  // private methods -  !!!put the "let" keyword!!!
  ////////////////////////////////////////////////////////////////
  let buildMapwall = () => {
    
    this.spinnerOn();
    getLocalResource(_configFile + "?nc=" + Date.now(), (x) => { //no cache
      window.globalConfig = JSON.parse(x)

      let geocfg = window.globalConfig.geo;
      if(!geocfg) throw("Missing geo configuration in config.json");

      let mapContainer = document.createElement('div')
      document.body.appendChild(mapContainer)
      mapContainer.classList = "mapcontainer"

      for(let i=0; i<geocfg.maps.length; i++) {
        let mapconfig = geocfg.maps[i];
        let mapId = mapconfig.id||"mw_map_"+i;

        let mapDiv = document.getElementById(mapId);

        //if no template is defined on the page all maps are displaced horizontally
        if(mapDiv == null) {
          mapDiv = document.createElement('div')
          mapDiv.setAttribute('id', mapId)
          mapDiv.style.width = '100%'
        

          if(geocfg.maps.length > 1) {
            mapDiv.style.width = 100/geocfg.maps.length + '%'
            if(i != geocfg.maps.length-1) 
              mapDiv.style.borderRight = '2px solid ' + softBG        
          }
          
          mapContainer.appendChild(mapDiv)
        }

        mapwall_maps[mapId] = new MWMap(mapconfig, mapId);
        
        /**
         * Mapwall Events
         */

        let that = this;

        //document.querySelector(".mapcontainer").addEventListener('dblclick', function(evt) {
        document.getElementsByTagName("body").item(0).addEventListener('dblclick', function(evt) {
          clearTimeout(mw_dblclick_timer);
          mw_prevent = true;
          let ctlmenu = document.getElementById("ctlmenu")
          that.repositioning(ctlmenu, evt)
        })
        
        /**
         * Emitted by MWMap after click
         */
        document.addEventListener('mwExecuteCallback', (e) => {
          if(e.mapwall_data.mapId == mapId) {
            e.mapwall_data.callbacks.forEach((cb) => {
              cb.pars[1].mapwall_data = {mapId: e.mapwall_data.mapId}; //per showPopup e callback
              
              let lm = that.getMap(mapId).getLayersMap();
              let lname = cb.pars[2]
              
              if((lm[lname].popup && lm[lname].callback || cb.callback == 'showPopup') && cb.pars.length == 3) {
                this.showPopup(cb.pars[0], cb.pars[1], cb.pars[2])
              }
              else if(cb.callback instanceof Function && cb.pars.length == 3) {
                cb.callback(cb.pars[0], cb.pars[1], cb.pars[2])
              }
            })
          }
        })
      } //foreach map

      showButtonsBar()
      showFixedPanels()

      ///////////////////////////////////////////////////////
      ///////////////////////////////////////////////////////
      // TODO WORKAROUND IPAD, TROVARE MODO DI ABILITARE INTERACT
      if(navigator.userAgent.match(/iPad/i) != null)
        document.getElementById('ctlmenu').classList.remove('draggable')
      ///////////////////////////////////////////////////////
      ///////////////////////////////////////////////////////
      document.dispatchEvent(new Event('mapwallReady'));
      
      this.spinnerOff();
      
    })
  }

  //Build the application boxes defined in config.json -> panels
  
  /**
   * [could be public]
   * Set the shared view to the one of the last moved map
   */
  let shareLastView = () => {
    if(_lastMovedMap) {
      shareViewByMapId(_lastMovedMap)
    }
  }


  /**
   * [could be public]
   * Set the shared view to the one of the map with the id passed as parameter
   */
  let shareViewByMapId = (mapId) => {
    let masterMap = mapwall.getMap(mapId)
    if(masterMap) {
      window.mw_shared_view = new ol.View(masterMap.getView().getProperties())

      Object.keys(mapwall_maps).forEach(k => {
        let m = mapwall_maps[k];
        if(m.getView() !== window.mw_shared_view) {
          m.setView(window.mw_shared_view)
        }
      })
    }
  }

  /**
   * [could be public]
   * Set the view of each map to an unshared copy of the current view
   */
  let unshareView = () => {
    Object.keys(mapwall_maps).forEach(k => {
      let m = mapwall_maps[k]
      m.setView(new ol.View(m.getView().getProperties()))
    })
  }

  let showFixedPanels = () => {
    if(globalConfig.panels && globalConfig.panels.length > 0) {
      masterPanel = document.createElement("div")
      masterPanel.classList = "popup100 mw_disableupdate fixedpanel"
        
      globalConfig.panels.forEach((pcfg) => {
        let horizontal = pcfg.position.orientation == "horizontal" || pcfg.position.orientation == "h" ? true : false;
        let previousOffset = (!horizontal && pcfg.position.vOffset ? pcfg.position.vOffset : 0);
        for(let i=1; i<=pcfg.instances; i++) {
          let panel = masterPanel.cloneNode(true)
          panel.id = pcfg.baseId + '_' + i;

          let size = (
            pcfg.customSize
            && pcfg.customSize[i]
            && pcfg.customSize[i].height
            && pcfg.customSize[i].width) ?  pcfg.customSize[i] : pcfg.size;

          panel.style.width = size.width;
          panel.style.height = size.height;
          panel.setAttribute("data-w-orig", size.width)
          panel.setAttribute("data-h-orig", size.height)
          if(pcfg.classes) panel.classList += ' '+pcfg.classes; //customization css classes

          let pos = pcfg.spacing + previousOffset;
          switch(pcfg.position.hAlign) {
            case "left":
              panel.style.left = horizontal ? pos : pcfg.spacing;
              if(horizontal) previousOffset = pos + size.width;
              break;
            case "right":
              panel.style.left = window.innerWidth - ((horizontal ? pos : pcfg.spacing) + size.width);
              if(horizontal) previousOffset = pos + size.width;
              break;
            default:
              panel.style.left = pos;
              previousOffset = pos + size.width; //default horizontal
              break;
          }

          switch(pcfg.position.vAlign) {
            case "top":
                panel.style.top = (horizontal ? pcfg.spacing + (pcfg.position.vOffset ? pcfg.position.vOffset : 0) : pos);
                if(!horizontal) previousOffset = pos + size.height;
                break;
            case "bottom":
                panel.style.top = window.innerHeight - ((horizontal ? pcfg.spacing  + (pcfg.position.vOffset ? pcfg.position.vOffset : 0) : pos) + size.height);
                if(!horizontal) previousOffset = pos + size.height;
                break;
            default:
                panel.style.top = pcfg.spacing + (pcfg.position.vOffset ? pcfg.position.vOffset : 0);
                break;
          }
          
          panel.classList += ' draggable' + ((pcfg.resizable === true) ? ' resizable' : '');
          panel.style.display = "block";

          document.body.append(panel);

          if(window[pcfg.callback]) window[pcfg.callback](panel.id)
        }
      })      
    }
  }

  let showButtonsBar = () => {
    let ctlmenu = document.createElement("div")
    ctlmenu.id = "ctlmenu";
    ctlmenu.classList = "draggable"
    
    window.globalConfig.buttonBar.items.forEach(btnCfg => {
      if(btnCfg.visible == true) {
        let btn = document.createElement("div");
        btn.id = btnCfg.id;
        btn.classList = "mw-circle";

        let icon = document.createElement("span");
        icon.classList = btnCfg.iconClass
        btn.appendChild(icon);

        //callback singoli pulsanti definite in config.json
        if(btnCfg.callback && btnCfg.callback != null) {
          btn.addEventListener("click", () => {
            if(btnCfg.active && btnCfg.active == true)
              toggleCtlButtons(btn.id)

            if(btnCfg.activeIconClass && btnCfg.activeIconClass != '')
              toggleActiveClass(btn.id, btnCfg.iconClass, btnCfg.activeIconClass);

            let cb = functionFromString(btnCfg.callback)
            if(cb instanceof Function)
              cb(btn.id) //WARNING!!!!!! viene sempre passato l'id del pulsante, attenzione ai prototipi delle callback
            //window[btnCfg.callback]();
          })
        }

        ctlmenu.appendChild(btn)
      }
    })

    document.body.appendChild(ctlmenu)
    
    //center
    ctlmenu.style.left = (document.body.offsetWidth / 2) - (ctlmenu.offsetWidth / 2)
    ctlmenu.style.top = (document.body.offsetHeight) - (ctlmenu.offsetHeight + 40);
  }

  let getLocalResource = (url, callback) => {      
    if ( !window.XMLHttpRequest ) return;  
    
    var xhr = new XMLHttpRequest();  
    
    xhr.onload = function() {
      if ( callback && typeof( callback ) === 'function' ) {      
        callback( this.responseText );
      }
    }
      
    xhr.open( 'GET', url );
    xhr.responseType = 'text';
    xhr.send();
  }

  let getHtmlComponent = (name, cb) => {
    let parser = new DOMParser();
    let doc = parser.parseFromString(mapwall_components[name], "text/html")
    let el = doc.getElementById(name);
    if(cb) cb(el)
    return el
  }

  let expandPopup = (elementPopup) => {
    let origsize = elementPopup.getAttribute("data-size").split("x")
    if(elementPopup.getAttribute("data-expanded") && elementPopup.getAttribute("data-expanded") == 1) {
      elementPopup.style.width = origsize[0]
      elementPopup.style.height = origsize[1]
      elementPopup.removeAttribute("data-expanded")
    }
    else {
      elementPopup.style.width = origsize[0] * 2
      elementPopup.style.height = origsize[1] * 2
      elementPopup.setAttribute("data-expanded",1)
    }
  }

  let softBg = (elementPopup) => {
    let isSoft = elementPopup.getAttribute("data-soft") && elementPopup.getAttribute("data-soft") == "1"
    if(isSoft) {
      elementPopup.removeAttribute("data-soft")
      elementPopup.style.backgroundColor = normBtnBG;
    }
    else {
      elementPopup.setAttribute("data-soft",1)
      elementPopup.style.backgroundColor = softBG
    }
  }

  //return active status
  let toggleCtlButtons = (id) => {
    let el = document.getElementById(id)
    if(el.getAttribute("data-mw-active") == "true") {
      el.style.backgroundColor = normBtnBG;
      el.removeAttribute("data-mw-active")
      return false;
    }
    else {
      el.style.backgroundColor = activeBtnBG;
      el.setAttribute("data-mw-active", true);
      return true;
    }
  }
  
  let toggleActiveClass = (id, iconClass, activeIconClass) => {
    let el = document.getElementById(id)
    if(el.querySelector("span").classList == iconClass) {
      el.querySelector("span").classList = activeIconClass
    }
    else {
      el.querySelector("span").classList = iconClass
    }
  }

  //Spostare in map??
  let getServiceLayer = (mapId) => {
    return this.getMap(mapId).getLayerByName(SERVICE_LAYER_NAME);
  }

  let blinkWatchedFeatures = (popup) => {
    let mapId = popup.getAttribute("data-map-id");
    let layerName = popup.getAttribute("data-feature-type");
    let layer = this.getMap(mapId).getLayerByName(layerName);

    let f = null;
    if(layer instanceof ol.layer.Vector) {
      f = layer.getSource().getFeatureById(popup.getAttribute("data-feature-id"))
    }
    else if(layer instanceof ol.layer.Tile) {
      let lm = this.getMap(mapId).getLayersMap();
      if(lm[layerName].placemarker == true) {
        f = getServiceLayer(mapId).getSource().getFeatureById(popup.id)
      }
    }

    if(f == null) {
      console.warn("Blinking available only on Vector layers or Tile layers with placemarker")  
      return;
    }
      
    let x = 0;
    let intBlink = setInterval(() => {
      x += 500;

      if(x >= 4000) {
        clearInterval(intBlink);
        f.setStyle()
      }
      else {
        if(x % 1000 != 0) {
          f.setStyle(hiddenStyleMw);
        } 
        else {
          f.setStyle()
        } 
      }
    } ,500)
  }

  let getMasterList = () => {
    let masters = [];
    Object.keys(mapwall_maps).forEach(k => {
      let m = mapwall_maps[k];
      let lmap = m.getLayersMap()
      Object.keys(lmap).forEach(lk => {
        lm = lmap[lk]
        if(lm.popupId) masters.push(lm.popupId)
      })
    })

    return masters;
  }

  let removeServiceFeature = (popup) => {
    let lname = popup.getAttribute("data-feature-type")
    let fid = popup.id;
    let mapId = popup.getAttribute("data-map-id");
    let layer = this.getMap(mapId).getLayerByName(lname);
    if(layer instanceof ol.layer.Tile) {
      let sl = getServiceLayer(mapId)
      if(sl != null) {
        let src = sl.getSource()
        src.removeFeature(src.getFeatureById(fid))
      }
    }
  }

  let removePopups = (pars) => {
    try {
      if(pars == undefined || pars && pars.mapId) {
        this.getOpenPopups((pops) => {
          pops.forEach((p) => {
            if(pars && pars.mapId) {
              if(pars.mapId == p.getAttribute("data-map-id")) {
                p.remove();
                removeServiceFeature(p)
              }
            }
            else {
              p.remove()
              removeServiceFeature(p)
            }
          })
          this.toggleUnwatchedFeatures() //se chiudo tutti i popup ripristino eventuali hide di unwatched
        })
      }
      else if(pars && pars.popupId) {
        let id = pars.popupId.startsWith('#') ? pars.popupId.substring(1) : pars.popupId;
        let popupCandidate = document.getElementById(id);
        //if(popupCandidate != null) popupCandidate.remove()
        popupCandidate.remove()
        removeServiceFeature(popupCandidate)
      }
    } catch(e) {
      console.warn(e)
    }
  }

  ////////////////////////////////////////////////////////////////
  // constructor - must stay after called methods
  if(window.mapwall != undefined) throw "A Mapwall instance is already running";
  else buildMapwall()
  ////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////
}




////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
////////////////////////////////////////////////////////////////
function checkCallbackType(cb) {
  if(typeof cb == 'string') {
    cb = functionFromString(cb)
  }

  if(cb instanceof Function) return cb;

  else throw("Callback is not a function")
}

//restituisce l'oggetto funzione a partire da una stringa nome, come mapwall.toggleAppMenu
//che può essere usata nel config
function functionFromString(string) {
  let obj = undefined;
  let parts = string.split('.')
  try {
    parts.forEach(o => {
      if(obj == undefined && window[o]) obj = window[o];
      else if(obj[o] != undefined) obj = obj[o]
    })
  }
  catch(e) {
    console.warn("Function " + string + " not found")
  }

  return obj
}

function getUrlParameter(pName, url) 
{
  pName = pName.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]'); 
  var regex = new RegExp('[\\?&]' + pName + '=([^&#]*)'); 
  var results = regex.exec(url); 
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' ')); 
}

function perc2color(perc) {      
  var r, g, b = 0;
  if(perc < 50) {
    g = 255;
    r = Math.round(5.1 * perc);
    b = r
  }
  else {
    r = 255;
    g = Math.round(510 - 5.10 * perc);
    b = g
  } 
  var h = r * 0x10000 + g * 0x100 + b * 0x1;
  return '#' + ('000000' + h.toString(16)).slice(-6);
}

function dragMoveListener (event) {
  var target = event.target
  // keep the dragged position in the data-x/data-y attributes
  var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
  var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy

  // translate the element
  target.style.webkitTransform =
    target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)'

  // update the posiion attributes
  target.setAttribute('data-x', x)
  target.setAttribute('data-y', y)
}

// this is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener


/**
 * Helper classes for creating OL layers from WFS ()GeoJSON) and WMS services
 * These helpers are designed for fast and easy layers declarations, for more advanced features
 * refer to the OL documentation
 * 
 * IMPORTANT: for asynchronous services such as WFS you must call await init() after object construction
 * 
 * example:
 *  let layer = new MapwallWFSGeoJsonLayer(WFSURL, myOLStyle);
 *  await layer.init();
 * 
 */
 class MapwallLayer {
  getLayer() {return this.layer}

  async init() {
    return new Promise((resolve, reject) => {resolve()});
  }
}

class MapwallWFSGeoJsonLayer extends MapwallLayer {

  constructor(url, style) {
    super();
    this.url = url;
    this.layer = new ol.layer.Vector({
      style: style
    })
  }

  async init() {
    let that = this;
    return new Promise((resolve, reject) => {
      this.url += '&srsname=EPSG:3857'

      var xhr = new XMLHttpRequest();
      xhr.open('GET', that.url);
      var onError = function() {
        reject("Error")
      }
      xhr.onerror = onError;

      xhr.onload = function() {
        if (xhr.status == 200) {
          let format = new ol.format.GeoJSON();
          let geo = JSON.parse(xhr.responseText)
          let features = []

          geo.features.forEach(f => {
            let feature = format.readFeature(f)
            features.push(feature)
          })

          that.layer.setSource(new ol.source.Vector({features:features}))
          resolve();
        } 
        else {
          reject("ERROR");
        }
      }
      xhr.send();
    })
  }
}


class MapwallWMSTileLayer extends MapwallLayer{
  //style is the name of the geoserver custom style to apply
  constructor(url, style) {
    super();
    let params = {
      TILED: true,
      CRS: 'EPSG:3857',
      crossOrigin: 'anonymous'
    } 
    
    if(style != null && style != undefined && style.length > 0)
      params['STYLES'] = style;

    this.layer = new ol.layer.Tile({
      source: new ol.source.TileWMS({
        url: url,
        params: params
      })
    })
  }
}